import React, { Component } from 'react';
import Articles from './components/articles';
import Featured from './components/featured';
import Header from './components/header';

class App extends Component {
  componentDidMount() {
    fetch('http://localhost:8000/api/articles')
    .then(res => res.json())
    .then((data) => {
      this.setState({ articles: data })
    })
    .catch(console.log) 

    fetch('http://localhost:8000/api/articles/featured')
    .then(res => res.json())
    .then((data) => {
      this.setState({ featured: data })
    })
    .catch(console.log) 
  }
  state = {
    articles: [],
    featured: [],
  }
  render() {
    return (
      <div class="row">
        <div class="col col-sm-12">
          <Header />
        </div>
        <div class="col col-md-8 col-sm-12">
          <Articles articles={this.state.articles} />
        </div>
        <div class="col col-md-4 col-sm-12">

          <Featured featured={this.state.featured} />
        </div>

      </div>
    );
  }
}

export default App;