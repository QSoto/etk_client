import React from 'react'

const Featured = ({ featured }) => {
  return (
    <div>
      <center><h2>Destacados</h2></center>
      {featured.map((feature) => (
        <div class="card">
          <div class="card-body">
            <img src = {feature.field_image[0] ? feature.field_image[0].url : ''} class="img-fluid"/> 
            <h5 class="card-title">{feature.title[0].value}</h5>
            <h6 class="card-subtitle mb-2 text-muted"></h6>
          </div>
        </div>
      ))}
    </div>
  )
};

export default Featured