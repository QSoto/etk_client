import React from 'react'

const Articles = ({ articles }) => {
    const [showDetails, setShowDetails] = React.useState(false)
    const onClick = () => setShowDetails(!showDetails)
  return (
    <div>
      <center><h2>Notícias Recientes</h2></center>
      {articles.map((article) => (
        <div class="card">
          <div class="card-body">
            <img src = {article.field_image[0] ? article.field_image[0].url : ''} class="img-fluid"/> 
            <h5 class="card-title">{article.title[0].value}</h5>
            <p class="card-text"> { !showDetails && article.field_resumen[0] ? 
            <div dangerouslySetInnerHTML={{__html : article.field_resumen[0].processed }} /> 
            : ''}</p>
            <div class="card-text"> { showDetails ? 
            <div dangerouslySetInnerHTML={{__html : article.body[0].processed }} />
            : null }
            </div>
            <a onClick={onClick}>{!showDetails ? "Ver Noticia" : "Ocultar"}</a>

            
            
          </div>
        </div>
      ))}
    </div>
  )
};

export default Articles