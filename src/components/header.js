import React from 'react'
import Logo from '../assets/logo.png';

const Header = ({ featured }) => {
  return (
    <div class="text-center">
        <img src={Logo} alt="website logo" />
    </div>
  )
};

export default Header